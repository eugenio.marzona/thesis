#!/bin/bash

tcpdump -i lo udp port 5009 -vv -X > server.txt &
pid=$!

iperf3 -s -p 5009 --one-off
kill $pid
