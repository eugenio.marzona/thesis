#!/bin/bash

SERVER_IP=$1
HOST_IP=$(hostname -I)

tcpdump -i lo udp port 5010 -vv -X > client.txt &
pid=$!

iperf3 -c ${SERVER_IP} -p 5009 -B ${HOST_IP} --cport 5010 -t 10 -u
kill $pid
