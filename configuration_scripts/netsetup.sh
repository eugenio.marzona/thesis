#!/bin/bash
# Sets netconfig either for raspbian lite images or directly for SD volumes, check the
# supplied wpa_supplicant.conf and dhcpcd.conf files for more information.
#
# REQUIRES: kpartx

# Default Variables
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SSH=0
WPACONF="${SCRIPTDIR}/wpa_supplicant.conf"
DHCPCONF="${SCRIPTDIR}/dhcpcd.conf"


# Check if user has root privileges
if [ "$EUID" -ne 0 ];then
    echo "Please run this script as root"
    exit 1
fi

# Display usage
usage()
{
    echo "Usage:"
    echo "$0 IMAGE/ROOT [-h] [-w wpa_supplicant.conf] [-d dhcpcd.conf] [-s]"
    echo "  -h               display this help"
    echo "  IMAGE/ROOT       SD root or raspbian image (REQUIRED)"
    echo "  -w               wpa_supplicant.conf to use"
    echo "  -d               dhcpcd.conf to use"
    echo "  -s               Enable ssh on port 22"
    exit 2
}

ROOT=$1
shift

# Command line arguments
while getopts ":hw:d:s" opt;
do
  case $opt in
    h)
        usage
        ;;
    w)
        if [[ (-f $OPTARG) && ($OPTARG == *.conf) ]]; then
            ${WPACONF}=$OPTARG
        else
            echo "ERROR: Invalid wpa_supplicant.conf file, check specified path"
            exit 1
        fi
        ;;
    d)
        if [[ (-f $OPTARG) && ($OPTARG == *.conf) ]]; then
            ${DHCPCONF}=$OPTARG
        else
            echo "ERROR: Invalid dhcpcd.conf file, check specified path"
            exit 1
        fi
        ;;
    s)
        ${SSH}=1
        ;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
  esac
done

# Check if raspbian image or sd root
if [[ (-f $ROOT) && ($ROOT == *.img) ]]; then

    IMAGE=1

elif [[ (-d $ROOT) && (-f "${ROOT}/etc/dhcpcd.conf") && (-f "${ROOT}/etc/wpa_supplicant/wpa_supplicant.conf")]]; then

    IMAGE=0

elif [ "$ROOT" == "-h" ]; then

    usage

else

    echo "ERROR: Invalid path, please check usage."
    usage

fi

if [ "$IMAGE" == "1" ]; then

    # Mount images
    echo "Mounting image..."
    kpartx -as ${ROOT}
    mkdir -p /mnt/rasp_p{1,2}

    mount /dev/mapper/loop0p1 /mnt/rasp_p1
    mount /dev/mapper/loop0p2 /mnt/rasp_p2

    echo "Overwriting net-config files..."
    cp ${WPACONF} /mnt/rasp_p2/etc/wpa_supplicant/wpa_supplicant.conf
    cp ${DHCPCONF} /mnt/rasp_p2/etc/dhcpcd.conf

    if [ "$SSH" == "1" ]; then
        echo "Enabling SSH..."
        touch /mnt/rasp_p1/ssh
    fi

    umount /mnt/rasp_p{1,2}
    kpartx -d  ${ROOT}

    echo "All done, exiting."

else

    echo "Overwriting net-config files on SD..."
    cp ${WPACONF} "${ROOT}/etc/wpa_supplicant/wpa_supplicant.conf"
    cp ${DHCPCONF} "${ROOT}/etc/dhcpcd.conf"

    if [ "$SSH" == "1" ]; then
        # Missing setup for mounted SDs, unknown path at the moment of writing
    fi

    echo "All done, exiting."

fi