import os, sys, csv, argparse, paramiko

# Python 3 procedure to setup queueing disciplines on remote routers
# NOTES: requires paramiko, to install via pip:
#
#   sudo pip3 install paramiko

# Default password and username
USERNAME = "root"
PASSWORD = "admin"

parser = argparse.ArgumentParser(description='Reads remote routers ip addresses from csv and sets up queue disciplines.')
parser.add_argument('-c','--csv', help='csv with ip addresses', required=True)
parser.add_argument('-q','--queue', help='can be pfifo/htb/sfq', required=True)
args = parser.parse_args()

# Check if given csv exists ans converts it to array format
print('Reading csv...')
if not(os.path.isfile(args.csv)):
    print("Error: input plan not found")
    exit(1)

ips = []
with open(args.csv) as mycsv:
    reader = csv.reader(mycsv,delimiter=",")
    for row in reader:
        ips.append(row)

# Set up commands to Launch
if str(args.queue) == "sfq":

    cmd = "tc qdisc add dev eth0 root sfq perturb 10"

elif str(args.queue) == "pfifo":

    cmd = "tc qdisc add dev eth0 root pfifo"

elif str(args.queue) == "htb":

    cmd = "tc qdisc add dev eth0 root prio"

else:

    print("ERROR: ")
    exit(1)

# Launch procedure

print("Opening SSH connections...")

for ip in ips:
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname = str(ip[0]), username = USERNAME, password = PASSWORD)
    ssh.exec_command("tc qdisc del root dev eth0")
    ssh.exec_command(cmd)
    ssh.close()

print("SSH Connections closed, everything done.")
