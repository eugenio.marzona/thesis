This repository contains a set of tools and programs used for the tests submitted
in the thesis project "Analisi quantitativa di Reti di Calcolatori simulate"

In these sub-directories you will find:
- Scripts used to launch distribuited tcp tests
- Scripts used to set-up the network configuration of routers and End-hosts
- The precompiled version of the plotting tool, for macOS, and the source code
- The custom kernel image installed on the Raspberry pi 3b+
