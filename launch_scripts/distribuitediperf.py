import os, sys, csv, argparse, time, paramiko, scp, threading
from threading import Thread
from scp import SCPClient

# This script launches iperf3test.sh on remote hosts via ssh, then collects logs via scp.
# NOTES: Requires paramiko and scp, to install via pip:
#
#   sudo pip3 install paramiko
#   sudo pip3 install scp

# Default username and password 
USERNAME = "pi"
PASSWORD = "raspberry"
runtime = 10

# In this version no data is channelled to python in/out, ssh status is polled every seconds after tests runtime
def sshthread(command, target, usr, passwd, runtime):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname = target, username = usr, password = passwd)

    stdin, stdout, stderr = ssh.exec_command(command)

    time.sleep(runtime)

    timertimeout = 10

    while stdout.channel.exit_status_ready() == False:
        time.sleep(1)
        timertimeout = timertimeout - 1
        if timertimeout == 0:
            ssh.close()
            print("ERROR: Thread timed out!")
            exit(1)
        
    ssh.close()

parser = argparse.ArgumentParser(description='Reads testplan in csv format, starts iperf3 distribuited test and collects the results.')
parser.add_argument('-p','--plan', help='Testplan in csv format', required=True)
parser.add_argument('-t','--time', help='Test runtime (seconds)', required=False)
parser.add_argument('-a','--algorithm', help='Tcp congestion control algorithm (reno, cubic, ecc)', choices=['reno', 'cubic', 'bic'], required=False)
parser.add_argument('-o','--outputdir', help='Test outputdir', required=False)
#parser.add_argument('-c','--cleanup', help='if set, launches logs cleanup on remote hosts', action='store_true', required=False)
args = parser.parse_args()


# Check if given plan exists, converts it to array format
print('Reading Plan...')
if not(os.path.isfile(args.plan)):
    print("Error: input plan not found")
    exit(1)

testcouples = []
with open(args.plan) as mycsv:
    reader = csv.reader(mycsv,delimiter=",")
    for row in reader:
        testcouples.append(row)

# Set up commands to execute
servercmd = "sudo bash /home/pi/scripts/iperf3test.sh s"
clientcmd = "sudo bash /home/pi/scripts/iperf3test.sh c"
if not(args.algorithm is None):
    servercmd = servercmd + " -a " + str(args.algorithm)
    clientcmd = clientcmd + " -a " + str(args.algorithm)

if not(args.time is None):
    clientcmd = clientcmd + " -t " + str(args.time)
    runtime = int(args.time)

# Set up output paths
outpath = os.getcwd()
if not(args.outputdir is None):
    if not(os.path.isdir(args.outputdir)):
        os.mkdir(args.outputdir)
    outpath = args.outputdir

# Create an array to load the threads into
threads = []

print("Launching servers...")
# Launch servers
for couple in testcouples:
    process = Thread(target=sshthread, args=[servercmd, str(couple[0]), USERNAME, PASSWORD, runtime])
    process.start()
    threads.append(process)

print("Waiting a little bit 4 server setup...")
time.sleep(5)

print("Launching clients...")
# Launch clients
for couple in testcouples:
    clientstr = clientcmd + " -i " + str(couple[0])
    # Set target bandwidth
    if not(couple[2] is None):
        clientstr = clientstr + " -b " + str(couple[2])
    process = Thread(target=sshthread, args=[clientstr, str(couple[1]), USERNAME, PASSWORD, runtime])
    process.start()
    threads.append(process)

print("Waiting for the tests to end...")
# Wait for all threads to complete
for p in threads:
    p.join()

print("All threads done. Retreiving logs from remote hosts...")

# And this should scp everything back
for couple in testcouples:
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname = str(couple[0]), username = USERNAME, password = PASSWORD)
    scp = SCPClient(ssh.get_transport(), sanitize=lambda x: x)
    scp.get(remote_path = '/home/pi/scripts/*.txt', local_path = outpath)
    scp.get(remote_path = '/home/pi/scripts/*.out', local_path = outpath)
    
#    if (args.cleanup == True):
#        ssh.exec_command("sudo rm /home/pi/scripts/*.txt /home/pi/scripts/*.out")

    ssh.close()

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname = str(couple[1]), username = USERNAME, password = PASSWORD)
    scp = SCPClient(ssh.get_transport(), sanitize=lambda x: x)
    scp.get(remote_path = '/home/pi/scripts/*.txt', local_path = outpath)
    scp.get(remote_path = '/home/pi/scripts/*.out', local_path = outpath)

#    if (args.cleanup == True):
#        ssh.exec_command("sudo rm /home/pi/scripts/*.txt /home/pi/scripts/*.out")
        
    ssh.close()

print("All done, Goodbye")