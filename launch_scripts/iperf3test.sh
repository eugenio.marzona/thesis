#!/bin/bash
# launches a iperf3 tests (either in server or client mode), loads required kernel modules,
# binds ports to use for connections and generates performance logs using tcp probe.

# Default Variables
TYPE=""
BANDWIDTH=""
ALGO="cubic"
SERVER_PORT=5006
CLIENT_PORT=5008
TEST_ID=$(date '+%Y-%m-%d_%H%M')
SERVER_IP=$(hostname -I)
OUTPUTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TIME=10
HOST_IP=$(hostname -I)

# Check if user has root privileges
if [ "$EUID" -ne 0 ]
  then echo "Please run this script as root"
  exit 1
fi

# Set congestion algorithm
setalgorithm()
{
    if [[ ( "${ALGO}" != "reno" ) && ( "${ALGO}" != "cubic" ) ]]; then
        modprobe "tcp_${ALGO}"
        echo ${ALGO} > /proc/sys/net/ipv4/tcp_congestion_control
    else
        echo ${ALGO} > /proc/sys/net/ipv4/tcp_congestion_control
    fi

}

# Remove loaded modules and set default algorithm back to cubic
cleanup()
{
    if [[ ( "${ALGO}" != "reno" ) && ( "${ALGO}" != "cubic" ) ]]; then
        modprobe -r "tcp_${ALGO}"
        echo cubic > /proc/sys/net/ipv4/tcp_congestion_control
    else
        echo cubic > /proc/sys/net/ipv4/tcp_congestion_control
    fi

    modprobe -r tcp_probe

}

# Get and check host type
TYPE=$1;
shift
if [[ ( "$TYPE" != "c" ) && ( "$TYPE" != "s" ) ]]; then
    echo "ERROR: host type has to be s (server) or c (client)"
    exit 1
fi

# Display usage
usage()
{
 	echo "Usage:"
 	echo "$0 s/c [-i server ip] [-a algorithm] [-t time] [-o outputdir] [-S sport] [-C cport] [-b bandwidth]"
    echo "  -h               display this help"
    echo "  s/c              Host type, can either be server or client (required)"
    echo "  -i server_ip     Server ip, default is localhost"
    echo "  -a algorithm     can be reno/cubic/bic, default is cubic"
    echo "  -t time          specify test runtime"
    echo "  -o outputdir     specify an output folder"
    echo "  -b bandwidth     target bandwidth for TCP connections, default is unlimited"
    echo "  -S sport         Override default server port"
    echo "  -C cport         Override default client port"
    exit 2
}

# Command line arguments
while getopts ":hi:a:t:o:b:S:C:b:" opt;
do
  case $opt in
    h)
		usage
      	;;
    i)
        SERVER_IP=$OPTARG
		;;
	a)
      	if [[ ( "$OPTARG" != "reno" ) && ( "$OPTARG" != "cubic" ) && ( "$OPTARG" != "bic" ) && ( "$OPTARG" != "veno" ) ]]; then
            echo "ERROR: Invalid tcp algorithm, see help for more info"
            exit 1
        else
            ALGO=$OPTARG
            setalgorithm
        fi
      	;;
    o)
		if [ -d $OPTARG ]; then
			OUTPUTDIR=$OPTARG
		else
			mkdir -p $OPTARG
			OUTPUTDIR=$OPTARG
		fi
		;;
	t)
      	if [ $OPTARG > 0 ]; then
      		TIME=$OPTARG
      	else
            echo "ERROR: Invalid test time value"
            exit 1
      	fi
      	;;
	b)
      	if [ $OPTARG > 0 ]; then
      		BANDWIDTH="-b ${OPTARG}"
      	else
            echo "ERROR: Invalid bandwidth value"
            exit 1
      	fi
      	;;
	S)
      	if [ $OPTARG > 5000 ]; then
      		SERVER_PORT=$OPTARG
      	else
            echo "ERROR: Port number is invalid or in use"
            exit 1
      	fi
      	;;
	C)
      	if [ $OPTARG > 5000 ]; then
      		CLIENT_PORT=$OPTARG
      	else
            echo "ERROR: Port number is invalid or in use"
            exit 1
      	fi
      	;;
    \?)
        echo "Invalid option: -$OPTARG" >&2
        exit 1
        ;;
    :)
        echo "Option -$OPTARG requires an argument." >&2
        exit 1
        ;;
  esac
done

# launch the test and collect logs
if [ "${TYPE}" == "s" ]; then

    modprobe tcp_probe port=${SERVER_PORT} full=1

    cat /proc/net/tcpprobe > "${OUTPUTDIR}/${TEST_ID}_SERVER_${HOSTNAME}.out" &
    pid=$!
    iperf3 -s -p ${SERVER_PORT} --one-off --logfile "${OUTPUTDIR}/${TEST_ID}_SERVER_${HOSTNAME}.txt"
    kill $pid

    cleanup
    exit 0

else

    modprobe tcp_probe port=${CLIENT_PORT} full=1

    cat /proc/net/tcpprobe > "${OUTPUTDIR}/${TEST_ID}_CLIENT_${HOSTNAME}.out" &
    pid=$!
    iperf3 -c ${SERVER_IP} -p ${SERVER_PORT} -B ${HOST_IP} --cport ${CLIENT_PORT} -t ${TIME} --logfile "${OUTPUTDIR}/${TEST_ID}_CLIENT_${HOSTNAME}.txt" ${BANDWIDTH}
    kill $pid

    cleanup
    exit 0

fi